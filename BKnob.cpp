/*
 * Copyright 2011 Davide Gessa. 
 * All rights reserved. Distributed under the terms of the MIT license.
 *
 * Authors:
 *				David Gessa, dak.linux@gmail.com
 */
#include "BKnob.h"

#include <stdio.h>
#include <Layout.h>
#include <Message.h>
#include <math.h>

BKnob::BKnob(const char *name, BMessage *message, int minValue, 
	int maxValue, int diameter)
	:	
	BSlider(BRect(100,100,500,500), name, NULL, message, minValue, maxValue)
{
	fDiameter = diameter;
	SetViewColor(ViewColor());
}

void
BKnob::Draw(BRect updateRect)
{
	// Get Max and Min values
	int32 maxValue, minValue;
	GetLimits(&minValue, &maxValue);
	
	int d = fDiameter;
	ResizeTo(d+2, d+2);
		
	// Draw the Ellipses
	StrokeEllipse(BRect(1, 1, d+1, d+1), B_SOLID_HIGH);
	SetHighColor(150, 150, 150, 255);
	FillEllipse(BRect(1, 1, d+1, d+1), B_SOLID_HIGH);
	SetHighColor(100, 100, 100, 255);
	FillEllipse(BRect(4, 4, d-2, d-2), B_SOLID_HIGH);
		
	// Limits
	StrokeLine(BPoint(d/2 + 1, d/2 + 1),
				BPoint(d/2 - d/4 - 1, d/2 + d/4 + 1),
				B_SOLID_HIGH);
	StrokeLine(BPoint(d/2 + 1, d/2 + 1),
				BPoint(d/2 + d/4 + 1, d/2 + d/4 + 1),
				B_SOLID_HIGH);
		
	BPoint start;
	BPoint end;
	double rad;
	uint32 diff = maxValue - minValue;
		
	// Transform fValue to radians
	rad = (5/3 * M_PI) - BSlider::Value() * (7/3 * M_PI) / diff;
	rad *= 2;
		
	// Determine indicator's position
	start = BPoint(d/2+1, d/2+1);
	end = BPoint((d/2+1)+d/2*sin(rad), (d/2+1)+d/2*cos(rad));
	SetPenSize(0.2f);
	SetHighColor(200,10,10,255);
	StrokeLine(start, end, B_SOLID_HIGH);
}

void
BKnob::MouseDown(BPoint point)
{
	if (!IsEnabled())
		return;
	printf("MouseDown() called\n");
	MakeFocus();
	fStartPoint = point;
	int32 newValue;
	newValue = BSlider::ValueForPoint(point);
	BSlider::SetValue(newValue);
	Invalidate();
}

void
BKnob::MouseUp(BPoint point)
{
	printf("MouseUp() called\n");
}

void
BKnob::MouseMoved(BPoint point, uint32 transit, const BMessage *message)
{
	if (!IsEnabled())
		return;
	printf("MouseMoved() called\n");
	
	// Get Max and Min values
	int32 maxValue, minValue;
	GetLimits(&minValue, &maxValue);
	
	uint32 buttons;
	GetMouse(&point, &buttons, true);
	BPoint fCurrentPoint;
	fCurrentPoint = point;
	if (buttons & B_PRIMARY_MOUSE_BUTTON) {
		if (fCurrentPoint.x > fStartPoint.x || fCurrentPoint.x < fStartPoint.x) {
			if (BSlider::Value() == minValue) {
				return;
			} else if (BSlider::Value() == maxValue) {
				return;
			} else {
				int32 newValue;
				newValue = BSlider::ValueForPoint(point);
				BSlider::SetValue(newValue);
				Invalidate();
			}
		} 
	}
}
