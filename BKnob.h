/*
 * Copyright 2011 Davide Gessa.
 * All rights reserved. Distributed under the terms of the MIT license.
 *
 * Authors:
 *				David Gessa, dak.linux@gmail.com
 */
#ifndef BKNOB_H
#define BKNOB_H

#include <Control.h>
#include <Slider.h>

class BKnob : public BSlider {
public:
			BKnob(const char *name, BMessage *message, int minValue,
											int maxValue, int diameter);
	void	Draw(BRect updateRect);
	void	MouseDown(BPoint point);
	void	MouseUp(BPoint point);
	void	MouseMoved(BPoint point, uint32 transit, 
								const BMessage *message);
		
private:
	int32 	fDiameter;
	BPoint	fStartPoint;

};

#endif 
