#include "MainWindow.h"

#include "BKnob.h"
#include <Application.h>
#include <Message.h>
#include <Slider.h>

MainWindow::MainWindow(void)
	:	BWindow(BRect(100,100,500,400),"Main Window",B_TITLED_WINDOW, B_ASYNCHRONOUS_CONTROLS)
{
	BMessage *msg = new BMessage;
	BKnob *knob = new BKnob("knob", msg, 0, 10, 100);
	AddChild(knob);
}


void
MainWindow::MessageReceived(BMessage *msg)
{
	switch (msg->what)
	{
		default:
		{
			BWindow::MessageReceived(msg);
			break;
		}
	}
}


bool
MainWindow::QuitRequested(void)
{
	be_app->PostMessage(B_QUIT_REQUESTED);
	return true;
}
